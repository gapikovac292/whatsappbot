<?php


namespace App\Utils;


use phpDocumentor\Reflection\Types\Self_;

class Workflow
{
    private static function questions(): array
    {
        return array(
            'questions' =>
                array(
                    0 =>
                        array(
                            'id' => 1,
                            'key_word' => NULL,
                            'label' => 'Hello cher(e) client(e) ! Je m\'appelle [RightCare]. Je vais rapidement prendre en charge votre demande. Pour cela merci de répondre aux questions suivantes.',
                            'type' => 'none',
                            'reponses' =>
                                array(),
                            'continue' => true,
                            'next' => 2,
                        ),
                    1 =>
                        array(
                            'id' => 2,
                            'key_word' => 'ABONNEMENT',
                            'label' => 'Quel type d\'abonnement avez-vous ? (tapez 1) pour Prepaid ou (tapez 2) pour Postpaid',
                            'type' => 'choice',
                            'reponses' =>
                                array(
                                    0 =>
                                        array(
                                            'id' => 1,
                                            'code' => '1',
                                            'key' => 'Prepaid',
                                        ),
                                    1 =>
                                        array(
                                            'id' => 2,
                                            'code' => 2,
                                            'key' => 'Postpaid',
                                        ),
                                ),
                            'continue' => false,
                            'next' => NULL,
                        ),
                    2 =>
                        array(
                            'id' => 3,
                            'key_word' => NULL,
                            'label' => 'Super ! J\'ai bien noté que vous avez un abonnement [ABONNEMENT]',
                            'type' => 'none',
                            'reponses' =>
                                array(),
                            'continue' => true,
                            'next' => 4,
                        ),
                    3 =>
                        array(
                            'id' => 4,
                            'key_word' => 'DEMANDE',
                            'label' => 'Voulez-vous faire une réclamation (tapez 1) ou une requête (tapez 2) ?',
                            'type' => 'choice',
                            'reponses' =>
                                array(
                                    0 =>
                                        array(
                                            'id' => 1,
                                            'code' => 1,
                                            'text' => 'Réclamation',
                                        ),
                                    1 =>
                                        array(
                                            'id' => 2,
                                            'code' => 2,
                                            'text' => 'Requête',
                                        ),
                                ),
                            'continue' => 'true',
                            'next' => 5,
                        ),
                    4 =>
                        array(
                            'id' => 5,
                            'key_word' => NULL,
                            'label' => 'OK vous voulez faire une [DEMANDE]. Pour bien qualifier votre [DEMANDE] veuillez choisir une catégorie parmi la liste suivante et saisissez le numéro de catégorie.',
                            'type' => 'none',
                            'reponses' =>
                                array(),
                            'continue' => 'false',
                            'next' => NULL,
                        ),
                ),
        );
    }

    public static function retrieveQuestion($questionId): array
    {
        return self::questions()["questions"][$questionId];
    }

    private static function abonnements()
    {
        return array(
            'abonnements' =>
                array(
                    0 =>
                        array(
                            'id' => 1,
                            'code' => 1,
                            'value' => 'Prepaid',
                            'demandes' =>
                                array(
                                    0 =>
                                        array(
                                            'id' => 1,
                                            'code' => 1,
                                            'value' => 'Reclamation',
                                            'categories' =>
                                                array(
                                                    0 =>
                                                        array(
                                                            'id' => 1,
                                                            'value' => 'Internet data prépaid',
                                                        ),
                                                    1 =>
                                                        array(
                                                            'id' => 2,
                                                            'value' => 'MoovFoot Alert',
                                                        ),
                                                    2 =>
                                                        array(
                                                            'id' => 3,
                                                            'value' => 'Moovgame',
                                                        ),
                                                    3 =>
                                                        array(
                                                            'id' => 4,
                                                            'value' => 'Moovsauvegarde',
                                                        ),
                                                    4 =>
                                                        array(
                                                            'id' => 5,
                                                            'value' => 'Moov tones',
                                                        ),
                                                    5 =>
                                                        array(
                                                            'id' => 6,
                                                            'value' => 'Moov info sms',
                                                        ),
                                                    6 =>
                                                        array(
                                                            'id' => 7,
                                                            'value' => 'Story box',
                                                        ),
                                                    7 =>
                                                        array(
                                                            'id' => 8,
                                                            'value' => 'Moov Funplus',
                                                        ),
                                                    8 =>
                                                        array(
                                                            'id' => 9,
                                                            'value' => 'Video box',
                                                        ),
                                                    9 =>
                                                        array(
                                                            'id' => 10,
                                                            'value' => 'Me2U',
                                                        ),
                                                    10 =>
                                                        array(
                                                            'id' => 11,
                                                            'value' => 'MoovRechargement EVD',
                                                        ),
                                                    11 =>
                                                        array(
                                                            'id' => 12,
                                                            'value' => 'Cartes de recharge',
                                                        ),
                                                    12 =>
                                                        array(
                                                            'id' => 13,
                                                            'value' => 'Moov forfait',
                                                        ),
                                                    13 =>
                                                        array(
                                                            'id' => 14,
                                                            'value' => 'Numero complice',
                                                        ),
                                                    14 =>
                                                        array(
                                                            'id' => 15,
                                                            'value' => 'Moovcrédit secours',
                                                        ),
                                                    15 =>
                                                        array(
                                                            'id' => 16,
                                                            'value' => 'Epiq Nation',
                                                        ),
                                                    16 =>
                                                        array(
                                                            'id' => 17,
                                                            'value' => 'Voix prepaid ',
                                                        ),
                                                    17 =>
                                                        array(
                                                            'id' => 18,
                                                            'value' => 'Sms prepaid',
                                                        ),
                                                    18 =>
                                                        array(
                                                            'id' => 19,
                                                            'value' => 'Carte Sim/Kit 100 000 080',
                                                        ),
                                                    19 =>
                                                        array(
                                                            'id' => 20,
                                                            'value' => 'Changement de profil',
                                                        ),
                                                    20 =>
                                                        array(
                                                            'id' => 21,
                                                            'value' => 'Accès au call center',
                                                        ),
                                                    21 =>
                                                        array(
                                                            'id' => 22,
                                                            'value' => 'Pertubation réseau',
                                                        ),
                                                ),
                                        ),
                                    1 =>
                                        array(
                                            'id' => 1,
                                            'code' => 1,
                                            'value' => 'Requête',
                                            'categories' =>
                                                array(
                                                    0 =>
                                                        array(
                                                            'id' => 1,
                                                            'value' => 'Internet data prépaid',
                                                        ),
                                                    1 =>
                                                        array(
                                                            'id' => 2,
                                                            'value' => 'MoovFoot Alert',
                                                        ),
                                                    2 =>
                                                        array(
                                                            'id' => 3,
                                                            'value' => 'Moovgame',
                                                        ),
                                                    3 =>
                                                        array(
                                                            'id' => 4,
                                                            'value' => 'Moovsauvegarde',
                                                        ),
                                                    4 =>
                                                        array(
                                                            'id' => 5,
                                                            'value' => 'Moovtones',
                                                        ),
                                                    5 =>
                                                        array(
                                                            'id' => 6,
                                                            'value' => 'Moov info sms',
                                                        ),
                                                    6 =>
                                                        array(
                                                            'id' => 7,
                                                            'value' => 'Story box',
                                                        ),
                                                    7 =>
                                                        array(
                                                            'id' => 8,
                                                            'value' => 'Video box',
                                                        ),
                                                    8 =>
                                                        array(
                                                            'id' => 9,
                                                            'value' => 'Me2U',
                                                        ),
                                                    9 =>
                                                        array(
                                                            'id' => 10,
                                                            'value' => 'EVD/Rechargement par Telepin',
                                                        ),
                                                    10 =>
                                                        array(
                                                            'id' => 11,
                                                            'value' => 'Cartes de recharge',
                                                        ),
                                                    11 =>
                                                        array(
                                                            'id' => 12,
                                                            'value' => 'Moovforfait',
                                                        ),
                                                    12 =>
                                                        array(
                                                            'id' => 13,
                                                            'value' => 'Numero complice',
                                                        ),
                                                    13 =>
                                                        array(
                                                            'id' => 14,
                                                            'value' => 'Moovcrédit secours',
                                                        ),
                                                    14 =>
                                                        array(
                                                            'id' => 15,
                                                            'value' => 'Epiq Nation',
                                                        ),
                                                    15 =>
                                                        array(
                                                            'id' => 16,
                                                            'value' => 'Voix prepaid',
                                                        ),
                                                    16 =>
                                                        array(
                                                            'id' => 17,
                                                            'value' => 'Sms prepaid -',
                                                        ),
                                                    17 =>
                                                        array(
                                                            'id' => 18,
                                                            'value' => 'Carte Sim/Kit 100 000 017',
                                                        ),
                                                    18 =>
                                                        array(
                                                            'id' => 19,
                                                            'value' => 'Changement de profil',
                                                        ),
                                                    19 =>
                                                        array(
                                                            'id' => 20,
                                                            'value' => 'Pertubation réseau',
                                                        ),
                                                    20 =>
                                                        array(
                                                            'id' => 21,
                                                            'value' => 'Requisitions',
                                                        ),
                                                    21 =>
                                                        array(
                                                            'id' => 22,
                                                            'value' => 'Renseignements/Suggestions',
                                                        ),
                                                    22 =>
                                                        array(
                                                            'id' => 23,
                                                            'value' => 'IRM/MPOS',
                                                        ),
                                                    23 =>
                                                        array(
                                                            'id' => 24,
                                                            'value' => 'Ventes de terminaux',
                                                        ),
                                                    24 =>
                                                        array(
                                                            'id' => 25,
                                                            'value' => 'Dépôt de courriers',
                                                        ),
                                                    25 =>
                                                        array(
                                                            'id' => 26,
                                                            'value' => 'Autres',
                                                        ),
                                                    26 =>
                                                        array(
                                                            'id' => 27,
                                                            'value' => 'Portabilité 1000000096',
                                                        ),
                                                    27 =>
                                                        array(
                                                            'id' => 28,
                                                            'value' => 'GreenWin 100 000 98',
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                    1 =>
                        array(
                            'id' => 2,
                            'code' => 2,
                            'value' => 'Postpaid',
                            'demandes' =>
                                array()
                        ),
                ),
        );
    }

    public static function getCategories($abonnement_id, $demande_id) {
        return self::abonnements()["abonnements"][$abonnement_id - 1]["demandes"][$demande_id - 1]["categories"];
    }

    public static function retrieveCategorie($abonnement_id, $demande_id, $categorie_id) {
        return self::abonnements()["abonnements"][$abonnement_id - 1]["demandes"][$demande_id - 1]["categories"][$categorie_id];
    }
}
