<?php


namespace App\Utils;


class Tools
{
    public static function callApi($headers, $args, $url, $method)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if ($method === "POST" || $method == "PUT") {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, $method == "POST" ? CURLOPT_POST : CURLOPT_PUT, true);

            if (!empty($args)) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args));
            }
        } else /* $method === 'GET' */ {
            if (!empty($args)) {
                curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($args));
            } else {
                curl_setopt($ch, CURLOPT_URL, $url);
            }
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // Make sure we can access the response when we execute the call
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($ch);

        if ($data === false) {
            //print_r(array('error' => 'API call failed with cURL error: ' . curl_error($ch)));
        }

        curl_close($ch);
    }
}
