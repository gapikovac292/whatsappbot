<?php

namespace App\Http\Controllers;

use App\Utils\Tools;
use App\Utils\Workflow;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    const API_URL = "https://api.gupshup.io/sm/api/v1/msg";
    const SOURCE_NUMBER = "917834811114";
    const HEADERS = [
        "apikey: c2f93b720f1e4e69cea029623ef37bcb",
        "Content-Type: application/x-www-form-urlencoded"
    ];

    public $customerInformation;
    public $user;
    public $toSend;
    public $error;

    public function __construct()
    {
        $this->customerInformation = null;
        $this->user = null;
        $this->toSend = null;
        $this->error = false;
    }

    public function index()
    {
        Cache::forget("22509820548");
        return "hello world";
    }

    public function webHook(Request $request)
    {
        $this->toSend = [
            "channel" => "whatsapp",
            "source" => self::SOURCE_NUMBER,
            "destination" => $request->payload["source"],
            "src.name" => $request->app,
        ];

        switch ($request->type) {
            case "message":
                //on recupère la saisie de l'utilisateur
                $message = $request->payload["payload"]["text"];

                //on recupère les informations du client dans le cache pour pouvoir suivre les process des questions
                $this->user = $request->payload["sender"];

                //on verifie que l'utilisateur a déjà entamé une conversation avec le bot
                if (Cache::has($this->user["phone"]))
                    $this->customerInformation = json_decode(Cache::get($this->user["phone"]));
                else // on initialise une vatiable dans le cache avec comme clé son numéro de telephone
                    $this->initCacheUser();


                //Evolution du questionnaire

                //Etape 0 (represente l'etape 0) lol
                //Salutation et affichage des abonnements (Prepaid ou Postpaid)
                if ($this->customerInformation->step == 0) {
                    $this->greeting();
                    $this->step1();
                }

                //Etape 1 (represente l'etape 2) lol
                //On recupère le choix de l'abonnement par l'utilisateur et affichage les demandes
                if ($this->customerInformation->step == 1) {
                    //le choix de l'utilisateur doit être 1 ou 2
                    if (in_array((int)$message, [1, 2]))
                        $this->step2($message);
                    else
                        $this->showErrorMessage();
                }

                //Etape 2 (represente l'etape 3) lol
                //On recupère le choix de la demande de l'utilisateur et affichage les categories associées
                if ($this->customerInformation->step == 2) {
                    //le choix doit être entre 1 et 2 (Reclamation ou Requête)
                    if (in_array((int)$message, [1, 2])) {
                        $this->step3($message);
                        $this->showCategories();
                    } else
                        $this->showErrorMessage();
                }

                //On récupère le choix de la categorie de l'utilisateur
                if ($this->customerInformation->step == 3) {
                    // le choix doit être compris entre 1 et le nombre total de catégorie disponible pour sa demande
                    if ((int)$message >= 1 && (int)$message <= $this->customerInformation->total_categories)
                        $this->step4($message);
                    else
                        $this->showErrorMessage();
                }

                //S'il n'y a pas d'erreur on incrémente le step pour passer à une question au prochain message de
                // l'utilisateur
                if (!$this->error) {
                    $this->customerInformation->step = (int)$this->customerInformation->step + 1;
                    Cache::put($this->user["phone"], json_encode($this->customerInformation));
                }

                break;
            case "user-event":
            case "message-event":
                //
                break;

        }
    }


    private function greeting()
    {
        $message = "Hello cher(e) client(e) ! Je m'appelle [RightCare]. Je vais rapidement prendre en charge votre demande. Pour cela merci de répondre aux questions suivantes.";
        $payload = json_encode([
            "isHSM" => "true",
            "type" => "text", //on peut specifier plusieurs types audio/video/image et ajouter un autre paramètre url (voir doc)
            "text" => $message
        ]);
        $this->toSend["message"] = $payload;
        Tools::callApi(self::HEADERS, $this->toSend, self::API_URL, "POST");
    }

    private function step1()
    {
        $message = "Quel type d'abonnement avez-vous ? 1 pour Prepaid et 2 pour Postpaid";
        $payload = json_encode([
            "isHSM" => "true",
            "type" => "text",
            "text" => $message
        ]);
        $this->toSend["message"] = $payload;
        Tools::callApi(self::HEADERS, $this->toSend, self::API_URL, "POST");
    }

    private function step2($abonnement)
    {
        $text = $abonnement == 1 ? "Prepaid" : "Postpaid";
        $this->customerInformation->abonnement = ["id" => $abonnement, "value" => $text];
        print_r("Super ! J'ai bien noté que vous avez un abonnement {$text}. Voulez-vous faire une réclamation (tapez 1) ou une requête (tapez 2) ?");
    }

    public function step3($requete)
    {
        $text = $requete == 1 ? "Reclamation" : "Requête";
        $this->customerInformation->demande = ["id" => $requete, "value" => $text];
        print_r("OK vous voulez faire une {$text}. Pour bien qualifier votre {$text} veuillez choisir une catégorie parmi la liste suivante et saisissez le numéro de catégorie.");
    }

    public function showCategories()
    {
        $ab = $this->customerInformation->abonnement->id;
        $dem = $this->customerInformation->demande["id"];
        $categories = Workflow::getCategories($ab, $dem);
        $this->customerInformation->total_categories = count($categories);
        $message = "\n\n";
        foreach ($categories as $key => $c) {
            $message .= ($key + 1) . " - {$c["value"]} \n";
        }
        print_r($message);
    }

    public function step4($categorie)
    {
        $ab = $this->customerInformation->abonnement->id;
        $dem = $this->customerInformation->demande->id;
        $this->customerInformation->categorie = Workflow::retrieveCategorie($ab, $dem, (int)$categorie - 1);
        print_r("Vous avez choisi la catégorie : {$this->customerInformation->categorie["value"]}");
    }

    private function initCacheUser()
    {
        $this->customerInformation = ["step" => 0, "abonnement" => [], "demande" => [],
            "categorie" => [], "souscategorie" => [], "total_categories" => 0];
        Cache::put($this->user["phone"], json_encode($this->customerInformation));
    }

    // cette méthode n'est pas encore bien gérée
    private function showErrorMessage()
    {
        $this->error = true;
        print_r("Désolé ! Nous n'avons pas saisi votre réponse. Veuilez réessayer svp");
    }
}
